//This streamer uses a modified version of Rishi556's streamer with node switching, an implementation of which can be found at https://github.com/Rishi556/LEOVOTER/blob/main/index.js 
//Said code is provided under MIT Licence

const config = require("../config.json");
const axios = require("axios");
const process = require("./process.js");

let errorCount = 0;
let nodes = config.nodes;
switchNode();
let nodeErrorSwitch = config.nodeErrorSwitch;

function getBlock(blockNumber) {
  let nextBlock = false;
  axios.post(currentNode, { "id": blockNumber, "jsonrpc": "2.0", "method": "call", "params": ["condenser_api", "get_block", [blockNumber]] }).then((res) => {
    if (res.data.result) {
      let block = res.data.result;
      nextBlock = true;
      parseBlock(block);
      errorCount = 0;
    }
  }).catch(() => {
  }).finally(() => {
    if (nextBlock) {
      setTimeout(() => {
        getBlock(blockNumber + 1);
      }, 0.5 * 1000);
    } else {
      nodeError();
      setTimeout(() => {
        getBlock(blockNumber);
      }, 3 * 1000);
    }
  })
}

function nodeError() {
  errorCount++;
  if (errorCount === nodeErrorSwitch) {
    switchNode();
  }
}

function switchNode() {
  errorCount = 0;
  currentNode = nodes.shift();
  nodes.push(currentNode);
}

function parseBlock(block) {
  if (block.transactions.length !== 0) {
    let trxs = block.transactions;
    for (let i in trxs) {
      let trx = trxs[i];
      process.process(trx);
    }
  }
}

async function getStartStreamBlock() {
  return new Promise((resolve, reject) => {
    axios.post(currentNode, { "id": 0, "jsonrpc": "2.0", "method": "condenser_api.get_dynamic_global_properties", "params": [] }).then((res) => {
      if (res.data.result) {
        return resolve(res.data.result.last_irreversible_block_num);
      } else {
        switchNode();
        startStreaming();
        return reject();
      }
    }).catch(() => {
      switchNode();
      startStreaming();
      return reject();
    })
  })
}

async function start() {
  let startBlock = await getStartStreamBlock().catch(() => { return });
  getBlock(startBlock);
}

module.exports = {
  start
}